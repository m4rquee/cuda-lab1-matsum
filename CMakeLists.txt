cmake_minimum_required(VERSION 2.8)
project(matsum)

find_package(CUDA REQUIRED)

add_executable(matsum src/matrix_add.c)

#cuda_add_executable(matsum-gpu src/matrix_add_gpu.cu)
