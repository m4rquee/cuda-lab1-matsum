#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define T 32

__global__ void add2d(int *A, int *B, int *C, int N, int M) {
    int col = T * blockIdx.x + threadIdx.x;
    int row = T * blockIdx.y + threadIdx.y;

    if (row < N && col < M) {
        C[row * M + col] = A[row * M + col] + B[row * M + col];
    }
}

int main() {
    int *A, *B, *C;
    int *d_A, *d_B, *d_C;
    int i, j;

    //Input
    int linhas, colunas;

    scanf("%d", &linhas);
    scanf("%d", &colunas);

    int tamanho = sizeof(int) * linhas * colunas;

    //Alocando memória na GPU
    cudaMalloc(&d_A, tamanho);
    cudaMalloc(&d_B, tamanho);
    cudaMalloc(&d_C, tamanho);

    //Alocando memória na CPU
    A = (int *)malloc(tamanho);
    B = (int *)malloc(tamanho);
    C = (int *)malloc(tamanho);
    
    //Inicializar
    for (i = 0; i < linhas; i++)
        for (j = 0; j < colunas; j++)
            A[i * colunas + j] = B[i * colunas + j] = i + j;

    //Copiando memória para GPU
    cudaMemcpyAsync(d_A, A, tamanho, cudaMemcpyHostToDevice);
    cudaMemcpyAsync(d_B, B, tamanho, cudaMemcpyHostToDevice);

    dim3 dimGrid(ceil((float) colunas / T), ceil((float) linhas / T), 1);
    dim3 dimBlock(T, T, 1);
    add2d<<<dimGrid, dimBlock>>>(d_A, d_B, d_C, linhas, colunas);

    cudaMemcpy(C, d_C, tamanho, cudaMemcpyDeviceToHost);

    long long int somador = 0;
    //Manter esta computação na CPU
    for (i = 0; i < linhas; i++)
        for (j = 0; j < colunas; j++)
            somador += C[i * colunas + j];
    
    printf("%lli\n", somador);

    cudaFree(d_A); cudaFree(d_B); cudaFree(d_C);
    free(A); free(B); free(C);
    return 0;
}
